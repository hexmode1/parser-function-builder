#+title: Creating parser functions for MediaWiki
#+author: Mark A. Hershberger
#+date: 2021-04-24

This composer package---known as the Parser Function Builder (PFB)---is intended to make it easy to set up [[https://www.mediawiki.org/wiki/Manual:Parser_functions][MediaWiki parser functions]].

To use this package:
- decide what you are going to call your extension and the PHP namespace it will use
- decide how your parser function will be called and what it will do
- illustrate what you want your parser functions should do
- use the wikitext to html transformations to create run parser tests to verify your extension works as expected
- initialize your =composer.json= and =extension.json= files for your new MediaWiki extension
- make this package a requirement for your extension in its =composer.json=
- in your =extension.json=, set up the =ParserFirstCallInit= hook to call the =registerFunction= method in the class you have set up for each parser function
- write an =execute= method and a =getValue= method for each class that you have desigated for each parser function
- create a page on MediaWiki.org describing your extension

* Rewriting the PageAfterAndBefore extension
To provide an example of using this extension, I'll document how I rewrote the [[https://www.mediawiki.org/w/index.php?oldid=798055][archived PageAfterAndBefore extension]] for a client.

** Deciding what to call it and the PHP namespace
This part was easy since I was working with an existing extension.  The extension already has a name.

I'm not wedded to its existing class structure and it was written before namespaces became really used in MediaWiki, though, so I can use my standard namespace and class structure.

MediaWiki extensions that I write are in the =MediaWiki\Extension\= namespace followed by the extension name.  That means this extension's namespace will be =MediaWiki\Extension\ParserAfterAndBefore=.

** Deciding what the parser function will do
Again, since I'm implementing an existing extension, this is easy.

The extension has four parser functions: =#pagebefore=, =#pageafter=, =#firstpage=, and =#lastpage=.  The parser functions will return relative (in the case of =#pagebefore= and =#pageafter=) or absolute (in the case of =#firstpage= and =#lastpage=) pages in a list (typically a category of pages).

The PFB requires that each parser function has its own class.  Since I'm going to use MediaWiki's camel-case convention for naming classes, this gives me (at least) the names =PageBefore=, =PageAfter=, =FirstPage=, and =LastPage= as the class names.

Let's use the example of publishing manuals in the wiki in a such a way that the all the pages in the manual are in a single MediaWiki category that acts as the manual's title.  For purposes of our example, let's use the category name "Extension manual".  The first page or "front matter" of our manual will be "Extension manual" page in the main namespace.  Other pages in the manual will be sub-pages of "Extension manual".

In our example manual, we'll only have three pages.  These will be:

- Extension manual
  - Extension manual/1 - Writing a parser function
  - Extension manual/2 - Writing a tag function

** Create illustrations of your parser functions
When organized this way, then, as long as the only pages in the category are manual pages, we can gurantee that ordering of the pages to make a book.  We can also say wan the parser functions should return when they are used on each page in this category.

The parser functions that return absolute pages (=#firstpage= and =#lastpage=) are easy since they'll always be the same for this set of pages:
+ {{#firstpage:category=Extension manual}} :: Extension manual
+ {{#lastpage:category=Extension manual}} :: Extension manual/2 - Writing a tag function

For the parser functions that return relative pages (=#pagebefore= and =#pageafter=), we have three different sets of results:

- Extension manual
  + {{#pagebefore:category=Extension manual|title=Extension manual}} :: (nothing)
  + {{#pageafter:category=Extension manual|title=Extension manual}} :: Extension manual/1 - Writing a parser function

- Extension manual/1 - Writing a Parser Function
  + {{#pagebefore:category=Extension manual|title=Extension manual/1 - Writing a Parser Function}} :: Extension manual
  + {{#pageafter:category=Extension manual|title=Extension manual/1 - Writing a Parser Function}} :: Extension manual/2 - Writing a tag function

- Extension manual/2 - Writing a tag function
  + {{#pagebefore:category=Extension manual|title=Extension manual/2 - Writing a tag function}} :: Extension manual/1 - Writing a Parser Function
  + {{#pageafter:category=Extension manual|title=Extension manual/2 - Writing a tag function}} :: (nothing)

** Write parser tests for the extension based on our illustrations of how it should work
We want to have something to check the functionality of our new extension as we write it, so we'll create some [[https://www.mediawiki.org/wiki/Parser_tests][parser tests]] based on the functionality we just described.

At this point, we do not expect to be able to accurately predict what MediaWiki's test results are going to be, we just want to have the test structure in place because MediaWiki's parser tests are not code---they're just text files that specify the state of some pages, some configuration, textual input for a test, and the expected textual output.

You can see the [[https://gitlab.com/hexmode1/page-after-and-before/-/blob/master/tests/parser/paab.txt][whole parser test file]], but, given what we've written in our example above, at this point we expect to define three article pages for our Extension manual, putting them in the category "Extension manual", a test for =#firstpage=, a test for =#lastpage=, and three different pairs of tests---one for each page---for =#pagebefore= and =#pageafter=.

Since we're just roughing out the tests right now, they're pretty simple.  A article definition looks like this:
#+begin_example
!! article
Extension manual/1 - Writing a parser function
!! text
[[Category:Extension manual]]
!! endarticle
#+end_example

And one of the tests looks like this:
#+begin_example
!! test
lastpage
!! wikitext
{{#lastpage:category=Extension manual}}
!! html
Extension manual/2 - Writing a tag function
!! end
#+end_example

** Set up extension.json and composer.json files

You can see the [[https://gitlab.com/hexmode1/page-after-and-before/-/blob/master/composer.json][full composer.json file]] if you want, but I'll only put the relevant, absolute minimum parts needed to use the PFB here:
#+begin_src json
  {
	  "name": "mediawiki/page-after-and-before",
	  "type": "mediawiki-extension",
	  "require": {
		  "composer/installers": "1.*,>=1.0.1",
		  "nichework/parser-function-builder": "*"
	  },
	  "autoload": {
		  "psr-4": {
			  "MediaWiki\\Extension\\PageAfterAndBefore\\": "include/"
		  }
	  }
  }
#+end_src

Most of these fields should be self-explanatory---especially the requirement for =nichework/parser-function-builder= without which this whole example would be made meaningless---but the =composer/installers= requirement is necessary to ensure that the extension is installed properly by composer.

Now, let's look at the MediaWiki-specific =extension.json= file.  Again, you can see the [[https://gitlab.com/hexmode1/page-after-and-before/-/blob/master/extension.json][full extension.json]] if you want, but for the purposes of illustrating the PFB, we'll only call out the relevant fields:
#+begin_src json
{
	"name": "PageAfterAndBefore",
	"Hooks": {
		"ParserFirstCallInit": [
			"MediaWiki\\Extension\\PageAfterAndBefore\\PageBefore::registerFunction",
			"MediaWiki\\Extension\\PageAfterAndBefore\\PageAfter::registerFunction",
			"MediaWiki\\Extension\\PageAfterAndBefore\\FirstPage::registerFunction",
			"MediaWiki\\Extension\\PageAfterAndBefore\\LastPage::registerFunction"
		]
	},
	"manifest_version": 2
}
#+end_src

Note that there is no reference in =extension.json= to the PFB and we are using composer's autoloader to find the classes for the extension.

Also, see the hooks we have for =ParserFirstCallInit=.  There is one =registerFunction= call for each of the parser functions.

** Write =execute()= and =getValue()= methods for each of the parser functions' classes
Finally, we're at the meat of the extension.

Since we're relying on PFB to handle almost all of our interaction with MediaWiki, we only have to write two methods for each class that implements a given parser function: =execute()= and =getValue()=.

These are divided up into separate methods so that we can throw errors while processing the =execute()= and allow PFB to report those issues and then still display any regular output if still appropriate.

*** Stubs only
To start, we'll use empty functions in our tests and see what the output gives us.  To make this easier still, we'll focus on the =#firstpage= parser function for now.

This gives us the following in our =include/FirstPage.php=:
#+begin_src php
  namespace MediaWiki\Extension\PageAfterAndBefore;

  use NicheWork\MW\ParserFunction;

  class FirstPage extends ParserFunction {
	  protected static $name = "firstpage";

	  public function execute() {
	  }

	  public function getValue() {
		  return var_export( $this->args, true );
	  }
  }
#+end_src

We've already discussed the namespace and classes.  The =FirstPage= class is extending =NicheWork\MW\ParserFunction= so that it can use the PFB that =ParserFunction= implements.  The one piece of glue that is needed is the =FirstPage::$name= static property so that the PFB will know which parser function this class is for.

Finally, we've implemented =getValue()= with a variation of [[https://stackoverflow.com/a/189570][printf() debugging]] so we can immediately begin to get a handle on what is going on in our method.

*** Install the extension and the PFB via composer.
We don't even need to register this extension with packagist.  Instead, we can tell composer where to find it by specifying the version control source in our =composer.local.json= in the top level of our MediaWiki installation:

#+begin_src json
{
	"require": {
		"mediawiki/page-after-and-before": "dev-master"
	},
	"repositories": [
		{
			"type": "vcs",
			"url": "https://gitlab.com/hexmode1/page-after-and-before"
		}
	]
}
#+end_src

From here, we can run =composer install= and add a
#+begin_src php
wfLoadExtension( "PageAfterAndBefore" );
#+end_src
to our =LocalSettings.php= file and we're ready to start a [[https://en.wikipedia.org/wiki/Read%E2%80%93eval%E2%80%93print_loop][REPL]] to help us build the rest of the extension.  We'll use the source directory installed under =$MW_INSTALL_PATH/extensions/PageAfterAndBefore= to continue doing our work.

*** Start running the extension and building on it.

We can run the parser test for the =#firstpage= and exercise our printf() debugging by running the parser tests in our =$MW_INSTALL_PATH= using the following command:
#+begin_src sh
$ cd $MW_INSTALL_PATH
$ tests/phpunit/phpunit.php --testsuite parsertests --stop-on-skipped \
	      --testdox --filter=first.page.tag
  Using PHP 7.3.27-1~deb10u1
  PHPUnit 8.5.15 by Sebastian Bergmann and contributors.

  Parser Integration
   ✘ Parse with paab.txt:·first·page·tag
	 ┐
	 ├ Failed asserting that two strings are equal.
	 ┊ ---·Expected
	 ┊ +++·Actual
	 ┊ @@ @@
	 ┊ -'Extension·manual'
	 ┊ +'<p>array·(\n
	 ┊ +</p>\n
	 ┊ +<pre>·'category'·=&gt;·'Extension·manual',\n
	 ┊ +</pre>\n
	 ┊ +<p>)\n
	 ┊ +</p>'
	 │
	 ╵ .../mediawiki/core/tests/phpunit/suites/ParserIntegrationTest.php:65
	 ╵ .../mediawiki/core/tests/phpunit/suites/SuiteEventsTrait.php:25
	 ╵ .../mediawiki/core/tests/phpunit/suites/SuiteEventsTrait.php:25
	 ╵ .../mediawiki/core/maintenance/doMaintenance.php:107
	 ┴

  Time: 1.04 seconds, Memory: 40.50 MB


  FAILURES!
  Tests: 1, Assertions: 1, Failures: 1.
#+end_src

This (expected) parser test failure for [[https://gitlab.com/hexmode1/parser-function-builder/-/blob/319b2812/README.org#L155][the stub]] shows us what PFB puts into our the =args= property when it reads the [[https://gitlab.com/hexmode1/page-after-and-before/-/blob/2bff62dc/tests/parser/paab.txt#L24][test identfied by "first page tag"]].  So, we can start creating our parser function in ernest.

*** Implement the =#firstpage= parser function using the category class
Since we're only dealing with categories at the moment, we can easily implement this using [[https://doc.wikimedia.org/mediawiki-core/master/php/classCategory.html][MediaWiki's category class]].
#+begin_src php
	public function execute() {
		if ( !isset( $this->args['category'] ) ) {
			throw new ParserFunctionException( "No category given" );
		}
		$category = Category::newFromName( $this->args['category'] );

		$titles = $category->getMembers( 1 );
		if ( count( $titles ) ) {
			$this->value =  $titles->current();
		}
		$this->final = true;
	}

	public function getValue() {
		return $this->value;
	}
#+end_src
First, we check to make sure we have a category.  If we don't then we throw an exception that PFB will catch.

Then, we ask for the first member of the category, put anything we get into =$this->value=, tell the PFB that we've done all the work needed (=$this->final = true;=) and then return the contents of =$this-value= when asked.

We run the tests again:
#+begin_src sh
$ cd $MW_INSTALL_PATH
$ tests/phpunit/phpunit.php --testsuite parsertests --stop-on-skipped \
	      --testdox --filter=first.page.tag
  Using PHP 7.3.27-1~deb10u1
  PHPUnit 8.5.15 by Sebastian Bergmann and contributors.

  Parser Integration
   ✘ Parse with paab.txt:·first·page·tag
	 ┐
	 ├ Failed asserting that two strings are equal.
	 ┊ ---·Expected
	 ┊ +++·Actual
	 ┊ @@ @@
	 ┊ -'Extension·manual'
	 ┊ +'<p>Extension·manual'\n
	 ┊ +</p>'
	 │
	 ╵ .../mediawiki/core/tests/phpunit/suites/ParserIntegrationTest.php:65
	 ╵ .../mediawiki/core/tests/phpunit/suites/SuiteEventsTrait.php:25
	 ╵ .../mediawiki/core/tests/phpunit/suites/SuiteEventsTrait.php:25
	 ╵ .../mediawiki/core/maintenance/doMaintenance.php:107
	 ┴

  Time: 1.22 seconds, Memory: 40.50 MB


  FAILURES!
  Tests: 1, Assertions: 1, Failures: 1.
#+end_src

This time, we can see that what we got is basically right.  We just need to [[https://gitlab.com/hexmode1/page-after-and-before/-/commit/eb19675be1f00e9050eebfd37aea975f50558bd3#7978063e494e1cd5b03fff2dbbfda6518cdb7c98][update our parser tests]] and run it again:
#+begin_src sh
$ cd $MW_INSTALL_PATH
$ tests/phpunit/phpunit.php --testsuite parsertests --stop-on-skipped \
	      --testdox --filter=first.page.tag
  Using PHP 7.3.27-1~deb10u1
  PHPUnit 8.5.15 by Sebastian Bergmann and contributors.

  Parser Integration
   ✔ Parse with paab.txt:·first·page·tag

  Time: 1.07 seconds, Memory: 40.50 MB

  OK (1 test, 1 assertion)
#+end_src


*** Implement the =#nextpage= parser function.
Since we've already done the =#firstpage= function we can do the =#nextpage= function in a similar way.
