<?php
/**
 * Copyright (C) 2021  NicheWork, LLC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Mark A. Hershberger  <mah@nichework.com>
 */
namespace NicheWork\MW;

use Html;
use MediaWiki\MediaWikiServices;
use MWException;
use Parser;
use PPFrame;
use PPNode;
use ParserOutput;

abstract class ParserFunction {
	/** @var ?string */
	protected static $name = null;
	/** @var bool */
	protected static $noHash = false;
	/** @var Parser */
	protected $parser;
	/** @var PPFrame */
	protected $frame;
	/** @var PPNode[] */
	protected $args;
	/** @var string[] */
	protected $error = [];
	/** @var ?string */
	protected $value = null;
	/** @var bool */
	protected $final;

	/**
	 * Any function that we've registered we will find here.
	 *
	 * @var array<string, string>
	 */
	private static $registeredFunction = [];

	/**
	 * Set up the tag handling.
	 *
	 * @param Parser $parser
	 */
	public static function registerFunction( Parser $parser ): void {
		$class = static::class;
		if ( static::$name === null ) {
			static::$name = $class;
			$sub = strrchr( $class, "\\");
			if ( $sub !== false ) {
				static::$name = substr( $sub, 1 );
			}
		}
		if ( static::$name !== null && static::$name !== false ) {
			$name = static::$name;

			if (
				isset( self::$registeredFunction[$name] ) &&
				self::$registeredFunction[$name] !== $class
			) {
				$class = self::$registeredFunction[$name];
				throw new MWException(
					sprintf(
						'Parser function %s is already handled by %s ... Cannot re-declare %s!',
						$name, $class, $class
					)
				);
			}
			$magic = 'MAG_' . strtoupper( $name );
			MediaWikiServices::getInstance()
				->getContentLanguage()->mMagicExtensions[$name] = [$magic, $name];
			$parser->setFunctionHook(
				$name, [ $class, 'functionHandler' ],
				Parser::SFH_OBJECT_ARGS | ( static::$noHash ? Parser::SFH_NO_HASH : 0 )
			);
			self::$registeredFunction[$name] = $class;
			return;
		}
		throw new MWException( sprintf( 'Need to set static name property for %s!', $class ) );
	}

	/**
	 * Handler for the parser function
	 *
	 * @param Parser $parser
	 * @param PPFrame $frame
	 * @param PPNode[] $args
	 * @return string
	 */
	public static function functionHandler( Parser $parser, PPFrame $frame, array $args ) {
		$function = new static( $parser, $frame, $args );

		return $function->asParsed();
	}

	/**
	 * Constructor for the parser function handler
	 *
	 * @param Parser $parser
	 * @param PPFrame $frame
	 * @param PPNode[] $args
	 */
	protected function __construct( Parser $parser, PPFrame $frame, array $args ) {
		$this->parser = $parser;
		$this->frame = $frame;
		$this->args = $this->parseArgs( $args );
	}

	/**
	 * Converts an array of values in form [0] => "name=value"
	 * into a real associative array in form [name] => value
	 * If no = is provided, true is assumed like this: [name] => true
	 *
	 * @param array string $options
	 * @return array $results
	 * @see https://www.mediawiki.org/w/index.php?oldid=4197226#Named_parameters
	 */
	protected function parseArgs( $options ) {
		$results = [];
		foreach ( $options as $option ) {
			$pair = array_map( 'trim', explode( '=', $this->frame->expand( $option ), 2 ) );
			if ( count( $pair ) === 2 ) {
				$results[ $pair[0] ] = $pair[1];
			}
			if ( count( $pair ) === 1 ) {
				$results[ $pair[0] ] = true;
			}
		}
		return $results;
	}

	/**
	 * Return the state of this tag as an HTML string.  Do not override this! Override getValue()
	 * instead!
	 *
	 * @return array
	 */
	final protected function asParsed(): array {
		$this->execute();
		/** @param string $error */
		$ret = implode( "\n", array_map( function ( $error ) {
			return sprintf( "<!-- %s -->", str_replace( "-->", "--/>", $error ) );
		}, $this->error ) ) . "";
		if ( $this->isFinal() ) {
			$ret .= $this->getValue();
		}

		return [ $ret, [
			'found' => $this->isFinal(),
			'nowiki' => !$this->isHTML(),
			'noparse' => !$this->isHTML(),
			'noargs' => $this->noSubstitution(),
			'isHTML' => $this->isHTML(),
		] ];
	}

	/**
	 * Returns true by default to indicate no more parsing should be done.
	 *
	 * @return bool
	 */
	protected function isFinal() {
		return $this->final;
	}

	/**
	 * Returns false by default to indicate that you want arguments like {{{1}}} replaced in the result.
	 *
	 * @return bool
	 */
	protected function noSubstitution() {
		return true;
	}

	/**
	 * Returns true by default to indicate that this is html that should not be parsed.
	 *
	 * @return bool
	 */
	protected function isHtml() {
		return true;
	}

	abstract public function execute();
	abstract public function getValue();
}
